# USB/BT IBM Model M/M2 Firmware
Uses Adafruit Feather NRF52840 Express module. Based off [BlueMicro firmware](https://github.com/jpconstantineau/BlueMicro_BLE). Heavily modified for USB support, MCP23017 GPIO expander support, ghost key prevention, lock LEDs, and more.

[BlueMicro Documentation](http://bluemicro.jpconstantineau.com/#)

---

### Building
* build.ps1 modelm:default -nrf52840 -verbose
* build.ps1 modelm2:default -nrf52840 -itsy -verbose
* build.ps1 modelmssk:default -nrf52840 -verbose

### Flashing
* flash840.bat modelm default master COM##
* flash840.bat modelm2 default master COM## _(doesn't work right now, just flash with UF2)_
* flash840.bat modelmssk default master COM##
