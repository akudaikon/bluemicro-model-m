#include <stdint.h>
#include "hid_keycodes.h"
#include "keyboard_config.h"
#include "advanced_keycodes.h"
#include "Key.h"
#include <array>

#ifndef KEYMAP_H
#define KEYMAP_H

#define NUM_LAYERS 3

#define _QWERTY 0
#define _L1  1
#define _L2  2

void setupKeymap();
extern std::array<std::array<Key, MATRIX_COLS>, MATRIX_ROWS> matrix;

void setupMatrix();

void setRow(uint8_t row);
void unsetRow(uint8_t row);
uint32_t readCols();

void matrixPrepareSleep();

#endif /* KEYMAP_H */
