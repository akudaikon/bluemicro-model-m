#include <bluefruit.h>
#include <Wire.h>
#include "keymap.h"

extern byte rows[];
extern byte columns[];

std::array<std::array<Key, MATRIX_COLS>, MATRIX_ROWS> matrix =
  {
    LAYOUT( /* Base layer */
      KC_ESC,           KC_F1,   KC_F2, KC_F3, KC_F4, KC_F5,  KC_F6, KC_F7, KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_PSCR, KC_SLCK, LAYER_1,
      KC_GRV,  KC_1,    KC_2,    KC_3,  KC_4,  KC_5,  KC_6,   KC_7,  KC_8,  KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSPC, KC_INS,  KC_HOME, KC_PGUP, KC_NLCK, KC_PSLS, KC_PAST, KC_PMNS,
      KC_TAB,  KC_Q,    KC_W,    KC_E,  KC_R,  KC_T,  KC_Y,   KC_U,  KC_I,  KC_O,    KC_P,    KC_LBRC, KC_RBRC, KC_BSLS, KC_DEL,  KC_END,  KC_PGDN, KC_P7,   KC_P8,   KC_P9,   KC_PPLS,
      KC_CAPS, KC_A,    KC_S,    KC_D,  KC_F,  KC_G,  KC_H,   KC_J,  KC_K,  KC_L,    KC_SCLN, KC_QUOT, KC_NUHS, KC_ENT,                             KC_P4,   KC_P5,   KC_P6,
      KC_LSFT, KC_NUBS, KC_Z,    KC_X,  KC_C,  KC_V,  KC_B,   KC_N,  KC_M,  KC_COMM, KC_DOT,  KC_SLSH,          KC_RSFT,          KC_UP,            KC_P1,   KC_P2,   KC_P3,   KC_PENT,
      KC_LCTL,          KC_LGUI,                      KC_SPC,                                 KC_RGUI,          KC_RALT, KC_LEFT, KC_DOWN, KC_RGHT, KC_P0,            KC_PDOT
    )
  };

void setupKeymap() 
{
  uint32_t layer1[MATRIX_ROWS][MATRIX_COLS] =
    LAYOUT( /* Alt layer */
      DFU,                 KC_NO,        KC_NO,        KC_NO,   KC_NO,      KC_NO,         KC_NO,      KC_NO,        KC_NO, KC_NO,     KC_NO, KC_NO, EEPROM_RESET, KC_NO,   KC_NO,   KC_NO,
      KC_NO, BLEPROFILE_1, BLEPROFILE_2, BLEPROFILE_3, KC_NO,   KC_NO,      KC_NO,         KC_NO,      KC_NO,        KC_NO, KC_NO,     KC_NO, KC_NO, KC_NO,        KC_MPRV, KC_MPLY, KC_MNXT, KC_NO, KC_NO, KC_NO, KC_BRID,
      KC_NO, KC_NO,        KC_NO,        KC_NO,        KC_NO,   KC_NO,      KC_NO,         CLEAR_BOND, KC_NO,        KC_NO, PRINT_BLE, KC_NO, KC_NO, KC_NO,        KC_MUTE, KC_MSTP, KC_NO,   KC_NO, KC_NO, KC_NO, KC_BRIU,
      KC_NO, KC_NO,        SLEEP_NOW,    KC_NO,        KC_NO,   KC_NO,      PRINT_HELP,    KC_NO,      KC_NO,        KC_NO, KC_NO,     KC_NO, KC_NO, KC_NO,                                   KC_NO, KC_NO, KC_NO,
      KC_NO, KC_NO,        KC_NO,        KC_NO,        KC_NO,   PRINT_INFO, PRINT_BATTERY, KC_NO,      MOUSE_JIGGLE, KC_NO, KC_NO,     PRINT_HELP,   KC_NO,                 KC_VOLU,          KC_NO, KC_NO, KC_NO, KC_NO,
      KC_NO,               KC_NO,                                           KC_NO,                                                     KC_NO,        KC_NO,        KC_MPRV, KC_VOLD, KC_MNXT, KC_NO,        KC_NO
    );

  /*
   * add the other layers
   */
  for (int row = 0; row < MATRIX_ROWS; ++row)
  {
    for (int col = 0; col < MATRIX_COLS; ++col)
    {
      matrix[row][col].addActivation(_L1, Method::PRESS, layer1[row][col]);
    }
  }
}

// Controls the MCP23017 16 pin I/O expander
static bool initialized;

#define I2C_ADDRESS 0x27

enum mcp23017_registers
{
	IODirectionA = 0x00,
  IODirectionB = 0x01,
  InputPolarityA = 0x02,
  InputPolarityB = 0x03,
  InterruptOnChangeA = 0x04,
  InterruptOnChangeB = 0x05,
  DefaultValueA = 0x06,
  DefaultValueB = 0x07,
  InterruptControlA = 0x08,
  InterruptControlB = 0x09,
  IOConfigurationA = 0x0a,
  IOConfigurationB = 0x0b,
  PullUpA = 0x0c,
  PullUpB = 0x0d,
  InterruptFlagA = 0x0e,
  InterruptFlagB = 0x0f,
  InterruptCaptureA = 0x10,
  InterruptCaptureB = 0x11,
  IOPortA = 0x12,
  IOPortB = 0x13,
  OutputLatchA = 0x14,
  OutputLatchB = 0x15,
};

static uint8_t reverse_byte(uint8_t x)
{
  // lazy, but fast
  static const uint8_t table[] =
  {
    0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0,
    0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
    0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
    0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
    0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4,
    0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
    0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec,
    0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
    0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
    0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
    0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea,
    0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
    0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6,
    0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
    0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
    0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
    0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1,
    0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
    0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9,
    0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
    0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
    0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
    0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed,
    0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
    0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3,
    0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
    0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
    0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
    0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7,
    0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
    0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef,
    0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff,
  };
  return table[x];
}

static void set_reg(enum mcp23017_registers reg, unsigned char val)
{
  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(reg);
  Wire.write(val);
  Wire.endTransmission();
}

void setupMatrix()
{
  // Initialize mcp23017
  initialized = false;

  Wire.begin();
  Wire.setClock(400000);

  // Set all the pins as inputs
  set_reg(IODirectionA, 0xff);
  set_reg(IODirectionB, 0xff);

  // Read key presses (logic low) as 0s
  set_reg(InputPolarityB, 0x00);
  set_reg(InputPolarityA, 0x00);

  // Turn on internal pull-ups
  set_reg(PullUpA, 0xff);
  set_reg(PullUpB, 0xff);

  // Disable interrupts
  set_reg(InterruptOnChangeA, 0x0);
  set_reg(InterruptOnChangeB, 0x0);

  initialized = true;

  // Initialize all the rows as INPUT_PULLUP
  for (int row = 0; row < MATRIX_ROWS; row++) pinMode(rows[row], INPUT_PULLUP);
}

void setRow(uint8_t row)
{
  // set the current row as OUPUT and LOW
  pinMode(rows[row], OUTPUT);
  digitalWrite(rows[row], LOW);
}

void unsetRow(uint8_t row)
{
  pinMode(rows[row], INPUT_PULLUP);
}

uint32_t readCols()
{
  uint16_t pins = 0;

  if (!initialized) return 0;

  uint8_t buf[2];

  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(IOPortA);
  Wire.endTransmission();

  Wire.requestFrom(I2C_ADDRESS, 2);
  buf[0] = Wire.read();  // PORTA, col 9-16
  buf[1] = Wire.read();  // PORTB, col 8-1
  buf[1] = reverse_byte(buf[1]);  // bits need to be reversed because col inputs reversed

  pins = (buf[0] << 8) | buf[1];
  return ~pins;
}

void matrixPrepareSleep()
{
  // Turn off internal pull-ups
  set_reg(PullUpA, 0x00);
  set_reg(PullUpB, 0x00);

  // Set all the pins as outputs
  set_reg(IODirectionA, 0x00);
  set_reg(IODirectionB, 0x00);

  // Set all the pins high
  set_reg(IOPortA, 0xFF);
  set_reg(IOPortB, 0xFF);

  // Set rows as INPUT, SENSE on high
  for (int j = 0; j < MATRIX_ROWS; ++j) pinMode(rows[j], INPUT_PULLDOWN_SENSE);
}
