#ifndef KEYBOARD_CONFIG_H
#define KEYBOARD_CONFIG_H

#include "hardware_variants.h"

#define ARDUINO_NRF52840_FEATHER

#define DEVICE_NAME_M     "IBM Model M2"             /**< Name of device. Will be included in the advertising data. */
#define DEVICE_MODEL      "Clacka Clacka McModelM2"  /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME "IBM"                      /**< Manufacturer. Will be passed to Device Information Service. */

#define KEYBOARD_SIDE MASTER

/* HARDWARE DEFINITION*/
/* key matrix size */
#define MATRIX_ROWS 10
#define MATRIX_COLS 18

#define MATRIX_HAS_GHOST

#define USE_LOCK_LEDS
  #define LED_NUM_LOCK_PIN      10
  #define LED_CAPS_LOCK_PIN     9
  #define LED_SCROLL_LOCK_PIN   7

  #define LOCK_LED_BRIGHTNESS   150 // set brightness of lock LEDs (i'm too lazy to open the keyboards and change the resistors)

#define BATTERY_TYPE  BATT_LIPO
#define VBAT_PIN      (A0)

// The keyboard matrix is attached to two MCP23017s, so these aren't used
#define MATRIX_COL_PINS { 0 };
#define MATRIX_ROW_PINS { 0 };

// MCP23017 INT (1 or 2) pin used for wakeup from sleep
#define MCP23017_INT_PIN  13

#define LAYOUT( \
    K5A,      K5B, K5C, K5D, K5E, K5F, K5G, K5H, K5I, K5J, K5K, K5L, K5M,   K5N, K5O, K5P, \
    \
    K4A, K4B, K4C, K4D, K4E, K4F, K4G, K4H, K4I, K4J, K4K, K4L, K4M, K4N,   K4O, K4P, K4Q,   K4R, K4S, K4T, K4U, \
    K3A, K3B, K3C, K3D, K3E, K3F, K3G, K3H, K3I, K3J, K3K, K3L, K3M, K3N,   K3O, K3P, K3Q,   K3R, K3S, K3T, K3U, \
    K2A, K2B, K2C, K2D, K2E, K2F, K2G, K2H, K2I, K2J, K2K, K2L, K2M, K2N,                    K2O, K2P, K2Q, \
    K1A, K1B, K1C, K1D, K1E, K1F, K1G, K1H, K1I, K1J, K1K, K1L,      K1M,        K1N,        K1O, K1P, K1Q, K1R, \
    K0A,      K0B,                K0C,                     K0D,      K0E,   K0F, K0G, K0H,   K0I,      K0J \
) \
{ \
/* 0 */ { K0J,   K0I,   K0I,   K1N,   K0B,   KC_NO, KC_NO, KC_NO, K5F,   K5G,   K2L,   KC_NO, K2G,   K2F,   K5E,   K1A,   K5A,   KC_NO }, \
/* 1 */ { K2Q,   K2P,   K2O,   K3U,   KC_NO, KC_NO, KC_NO, K1A,   K4N,   K3M,   K3L,   K5H,   K3G,   K3F,   K5D,   K2A,   K3A,   KC_NO }, \
/* 2 */ { KC_NO, KC_NO, KC_NO, K5P,   KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO }, \
/* 3 */ { KC_NO, KC_NO, KC_NO, KC_NO, K5O,   KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO }, \
/* 4 */ { K3Q,   KC_NO, KC_NO, K3P,   K5N,   K5M,   K5L,   KC_NO, K5K,   K4I,   K4K,   K4J,   K4H,   K4E,   K4D,   K4C,   K4B,   KC_NO }, \
/* 5 */ { K4Q,   K4O,   K3O,   K4P,   KC_NO, KC_NO, KC_NO, KC_NO, K5J,   K4M,   K4L,   K5I,   K4G,   K4F,   K5C,   K5B,   K4A,   K0A   }, \
/* 6 */ { K3T,   K3S,   K3R,   K3U,   KC_NO, KC_NO, KC_NO, KC_NO, K4N,   K3I,   K3K,   K3J,   K3H,   K3E,   K3D,   K3C,   K3B,   KC_NO }, \
/* 7 */ { K1Q,   K1P,   K1O,   K1R,   K1R,   KC_NO, KC_NO, KC_NO, K3N,   K2I,   K2K,   K2J,   K2H,   K2E,   K2D,   K2C,   K2B,   KC_NO }, \
/* 8 */ { K4T,   K4S,   K4R,   KC_NO, KC_NO, KC_NO, KC_NO, K1M,   K2N,   K1J,   K2N,   K1K,   K1I,   K1F,   K1E,   K1D,   K1C,   K0E   }, \
/* 9 */ { K4U,   K0H,   K0G,   K0F,   K0D,   KC_NO, KC_NO, KC_NO, K0C,   K1M,   K1L,   KC_NO, K1H,   K1G,   KC_NO, KC_NO, KC_NO, KC_NO }, \
}
/*        0,     1,     2,     3,     4,     5,     6,     7,     8,     9,     10,    11,    12,    13,    14,    15,    16,    17    */

// Corresponds to which columns have real keys associated with them (1) vs KC_NO (0)
// Used for anti-ghost algo
#define LAYOUT_REALKEYS \
{ \
/* row 00 */  0b011111011100011111, \
/* row 01 */  0b011111111110001111, \
/* row 02 */  0b000000000000001000, \
/* row 03 */  0b000000000000010000, \
/* row 04 */  0b011111111101111001, \
/* row 05 */  0b111111111100001111, \
/* row 06 */  0b011111111100001111, \
/* row 07 */  0b011111111100011111, \
/* row 08 */  0b111111111110000111, \
/* row 09 */  0b000011011100011111, \
}
/*          col ..FEDCBA9876543210 */

#endif /* KEYBOARD_CONFIG_H */
