/*
Copyright 2018-2020 <Pierre Constantineau, Julian Komaromy>

3-Clause BSD License

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
/**************************************************************************************************************************/

#include <bluefruit.h>
#include "firmware.h"
#include <Adafruit_LittleFS.h>
#include <InternalFileSystem.h>
#include "solenoid.h"

using namespace Adafruit_LittleFS_Namespace;
#define SETTINGS_FILE "/settings"

#define VERSION "4.0"

#ifdef MAC_KEYMAP
  #define FIRMWARE_VERSION "v" VERSION "M"
#else
  #define FIRMWARE_VERSION "v" VERSION
#endif

/**************************************************************************************************************************/
// Keyboard Matrix
byte rows[] MATRIX_ROW_PINS;        // Contains the GPIO Pin Numbers defined in keyboard_config.h
byte columns[] MATRIX_COL_PINS;     // Contains the GPIO Pin Numbers defined in keyboard_config.h

#ifdef MATRIX_HAS_GHOST
  uint32_t realKeys[] LAYOUT_REALKEYS;
#endif

SoftwareTimer keyscantimer, batterytimer;

File file(InternalFS);
PersistentState keyboardconfig;
DynamicState keyboardstate;

KeyScanner keys;
Battery batterymonitor;

static std::vector<uint16_t> stringbuffer; // buffer for macros to type into...

static uint8_t connectionState = 255;

static bool mouseJiggle = false;
static bool lastMouseJiggleDir = false;
static uint32_t mouseJiggleTimer = 0;

#ifdef USE_LOCK_LEDS
  static bool blink_state = true;
  static uint32_t blink_timer = 0;
  static bool battShownOnLEDs = false;
#endif

#ifdef USE_SPEAKER
  static uint8_t prevLockKeysPressed = 0;
#endif

static bool needReset = false;
static bool needUnpair = false;
static bool needFSReset = false;

/**************************************************************************************************************************/
void setupConfig()
{
  InternalFS.begin();
  loadConfig();
}

void loadConfig()
{
  file.open(SETTINGS_FILE, FILE_O_READ);

  if (file)
  {
    file.read(&keyboardconfig, sizeof(keyboardconfig));
    file.close();
  }
  else
  {
    resetConfig();
    saveConfig();
  }

  keyboardstate.timestamp = millis();
  keyboardstate.lastupdatetime = keyboardstate.timestamp;
  keyboardstate.timerkeyscaninterval = HIDREPORTINGINTERVAL;
  keyboardstate.timerbatteryinterval = 30 * 1000;
  keyboardstate.save2flash = false;
}

void saveConfig()
{
  InternalFS.remove(SETTINGS_FILE);

  if (file.open(SETTINGS_FILE, FILE_O_WRITE))
  {
    file.write((uint8_t*)&keyboardconfig, sizeof(keyboardconfig));
    file.close();
  }
}

void resetConfig()
{
  keyboardconfig.BLEProfile = 0;
  memset(keyboardconfig.BLEProfileAddr[0], 0, 6);
  memset(keyboardconfig.BLEProfileAddr[1], 0, 6);
  memset(keyboardconfig.BLEProfileAddr[2], 0, 6);
  strcpy(keyboardconfig.BLEProfileName[0], "unpaired");
  strcpy(keyboardconfig.BLEProfileName[1], "unpaired");
  strcpy(keyboardconfig.BLEProfileName[2], "unpaired");
}

/**************************************************************************************************************************/
// put your setup code here, to run once:
/**************************************************************************************************************************/
// cppcheck-suppress unusedFunction
void setup()
{
  setupConfig();
  // Serial.begin(115200);
  // while ( !Serial ) delay(10);   // for nrf52840 with native usb this makes the nrf52840 stall and wait for a serial connection.  Something not wanted for a keyboard...

  setupGpio();  // checks that NFC functions on GPIOs are disabled.

  // Configure WDT
  NRF_WDT->CONFIG         = 0x00;             // stop WDT when sleeping
  NRF_WDT->CRV            = 5 * 32768 + 1;    // set timeout (5 sec)
  NRF_WDT->RREN           = 0x01;             // enable the RR[0] reload register
  NRF_WDT->TASKS_START    = 1;                // start WDT

  keyscantimer.begin(keyboardstate.timerkeyscaninterval, keyscantimer_callback);
  batterytimer.begin(keyboardstate.timerbatteryinterval, batterytimer_callback);

  usb_setup();
  bt_setup(keyboardconfig.BLEProfile);

  setupKeymap();
  setupMatrix();

  bt_startAdv();

  keyscantimer.start();
  batterytimer.start();
  batterymonitor.updateBattery();

  //suspendLoop();
  stringbuffer.clear();

  #ifdef USE_SPEAKER
    playTone(TONE_STARTUP);
    playTone(TONE_BLE_PROFILE);
  #endif
};

void showBattOnLEDs(void)
{
  #ifdef USE_LOCK_LEDS
    batterymonitor.updateBattery();
    uint8_t intval = batterymonitor.vbat_per;

    if (intval > 75)        // 75% - 100%
    {
      analogWrite(LED_NUM_LOCK_PIN, 255);
      analogWrite(LED_CAPS_LOCK_PIN, 255);
      analogWrite(LED_SCROLL_LOCK_PIN, 255);
    }
    else if (intval > 50)   // 50% - 75%
    {
      analogWrite(LED_NUM_LOCK_PIN, 255);
      analogWrite(LED_CAPS_LOCK_PIN, 255);
      analogWrite(LED_SCROLL_LOCK_PIN, 255 * 0.2f);
    }
    else if (intval > 35)   // 35% - 50%
    {
      analogWrite(LED_NUM_LOCK_PIN, 255);
      analogWrite(LED_CAPS_LOCK_PIN, 255);
      analogWrite(LED_SCROLL_LOCK_PIN, 0);
    }
    else if (intval > 20)   // 20% - 35%
    {
      analogWrite(LED_NUM_LOCK_PIN, 255);
      analogWrite(LED_CAPS_LOCK_PIN, 255 * 0.2f);
      analogWrite(LED_SCROLL_LOCK_PIN, 0);
    }
    else if (intval > 10)   // 10% - 20%
    {
      analogWrite(LED_NUM_LOCK_PIN, 255);
      analogWrite(LED_CAPS_LOCK_PIN, 0);
      analogWrite(LED_SCROLL_LOCK_PIN, 0);
    }
    else                    // 0% - 10%
    {
      analogWrite(LED_NUM_LOCK_PIN, 255 * 0.2f);
      analogWrite(LED_CAPS_LOCK_PIN, 0);
      analogWrite(LED_SCROLL_LOCK_PIN, 0);
    }
  #endif
}

/**************************************************************************************************************************/
// Better scanning with debounce Keyboard Scanning
/**************************************************************************************************************************/
void scanMatrix()
{
  keyboardstate.timestamp = millis();   // lets call it once per scan instead of once per key in the matrix

  //take care when selecting debouncetime - each row has a delay of 1ms inbetween - so if you have 5 rows, setting debouncetime to 2 is at least 5ms...
  static uint32_t pindata[MATRIX_ROWS][DEBOUNCETIME];
  static uint8_t head = 0; // points us to the head of the debounce array
  uint32_t matrixData[MATRIX_ROWS];
  uint8_t keysPressed = 0;
  bool hasGhost = false;

  // Read matrix
  for (int j = 0; j < MATRIX_ROWS; ++j)
  {
    uint32_t pinreg = 0;

    setRow(j);
    nrfx_coredep_delay_us(100);            // need for the GPIO lines to settle down electrically before reading.
    pindata[j][head] = readCols();

    //debounce happens here - we want to press a button as soon as possible, and release it only when all bounce has left
    for (int d = 0; d < DEBOUNCETIME; ++d) pinreg |= pindata[j][d];
    pinreg &= ((1 << MATRIX_COLS) - 1);  // mask out non-column bits

#ifdef MATRIX_HAS_GHOST
    // set matrix data for row and mask out any matrix positions that aren't real keys
    // this is so we don't trigger anti-ghost for keys that wouldn't result in a valid keypress anyway
    matrixData[j] = pinreg & realKeys[j];
#else
    matrixData[j] = pinreg;
#endif

    for (; pinreg; keysPressed++) pinreg &= (pinreg - 1);  // keep count of number of keys pressed
    unsetRow(j);
  }

#ifdef MATRIX_HAS_GHOST
  // Check if ghost possible (only if greater than 2 keys pressed)
  if (keysPressed > 2)
  {
    for (uint8_t row = 0; row < MATRIX_ROWS; row++)
    {
      // Skip if no cols active in this row
      if (matrixData[row] == 0) continue;

      for (uint8_t i = 0; i < MATRIX_ROWS; i++)
      {
        // Skip if comparing same row or no cols active in row
        if (i == row) continue;
        if (matrixData[i] == 0) continue;

        // Ghost exists if row is in same state as another row
        if ((matrixData[i] & matrixData[row]) == matrixData[row])
        {
          hasGhost = true;
          break;
        }
      }
      if (hasGhost) break;
    }
  }
#endif

  // Press active keys if ghost not possible
  for (int j = 0; j < MATRIX_ROWS; ++j)
  {
    for (int i = 0; i < MATRIX_COLS; ++i)
    {
      if (!hasGhost)
      {
        int ulPin = (matrixData[j] >> i) & 1;
        if (ulPin) KeyScanner::press(keyboardstate.timestamp, j, i);
        else KeyScanner::release(keyboardstate.timestamp, j, i);
      }
    }
  }

  head++;
  if (head >= DEBOUNCETIME) head = 0; // reset head to 0 when we reach the end of our buffer
}

/**************************************************************************************************************************/
/**************************************************************************************************************************/
#if USER_MACRO_FUNCTION == 1
    void process_user_macros(uint16_t macroid)
    {
        switch ((macroid))
        {
            case MC(KC_A):
            addStringToQueue( "Macro Example 1");
            break;
        }
    }
#endif

/**************************************************************************************************************************/
// macro string queue management
/**************************************************************************************************************************/
void addStringToQueue(const char* str)
{
  auto it = stringbuffer.begin();
  char ch;
  while( (ch = *str++) != 0 )
  {
    uint8_t modifier = ( hid_ascii_to_keycode[(uint8_t)ch][0] ) ? KEYBOARD_MODIFIER_LEFTSHIFT : 0;
    uint8_t keycode = hid_ascii_to_keycode[(uint8_t)ch][1];
    uint16_t keyreport = MOD( modifier << 8 , keycode);
    it = stringbuffer.insert(it, keyreport);
  }
}

/**************************************************************************************************************************/
/**************************************************************************************************************************/
void addKeycodeToQueue(const uint16_t keycode)
{
  auto it = stringbuffer.begin();
  auto hidKeycode = static_cast<uint8_t>(keycode & 0x00FF);

  if (hidKeycode >= KC_A && hidKeycode <= KC_EXSEL)  // only insert keycodes if they are valid keyboard codes...
  {
      it = stringbuffer.insert(it, keycode);
  }
}

void addKeycodeToQueue(const uint16_t keycode, const uint8_t modifier)
{
  auto it = stringbuffer.begin();
  auto hidKeycode = static_cast<uint8_t>(keycode & 0x00FF);

  if (hidKeycode >= KC_A && hidKeycode <= KC_EXSEL)  // only insert keycodes if they are valid keyboard codes...
  {
      uint16_t keyreport = MOD(modifier << 8, hidKeycode);
      it = stringbuffer.insert(it, keyreport);
  }
}

/**************************************************************************************************************************/
/**************************************************************************************************************************/
void process_keyboard_function(uint16_t keycode)
{
  char buffer[64];

  switch(keycode)
  {
    case RESET:
      NVIC_SystemReset();
    break;

    case DEBUG:
    break;

    case EEPROM_RESET:
      needFSReset = true;
    break;

    case CLEAR_BOND:
      if (connectionState == CONNECTION_BT) needUnpair = true;
    break;

    case DFU:
      if (connectionState == CONNECTION_USB) 
      {
        #ifdef USE_SPEAKER
          playTone(TONE_SLEEP);
          playAllQueuedTonesNow();
        #endif
        enterUf2Dfu();
      }
    break;

    case SERIAL_DFU:
      #ifdef USE_SPEAKER
        playTone(TONE_SLEEP);
        playAllQueuedTonesNow();
      #endif
      enterSerialDfu();
    break;

    case PRINT_BATTERY:
      if (usb_isConnected())
      {
        snprintf(buffer, sizeof(buffer), "VDD = %lu mV, VBatt = %lu mV, %u %%", batterymonitor.vbat_vdd, batterymonitor.vbat_mv, batterymonitor.vbat_per);
      }
      else
      {
        sprintf(buffer, "VBatt = %lu mV, %u %%", batterymonitor.vbat_mv, batterymonitor.vbat_per);
      }
      addStringToQueue(buffer);
      addKeycodeToQueue(KC_ENTER);
      addKeycodeToQueue(KC_ENTER);
    break;

    case PRINT_INFO:
      addStringToQueue("Keyboard Name:   " DEVICE_NAME " "); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Keyboard Model:  " DEVICE_MODEL " "); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Keyboard Mfg:    " MANUFACTURER_NAME " "); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Firmware Ver:    " FIRMWARE_VERSION " "); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("BSP Library:     " ARDUINO_BSP_VERSION " "); addKeycodeToQueue(KC_ENTER);
      sprintf(buffer,  "Bootloader:      %s", getBootloaderVersion());
      addStringToQueue(buffer);
      addKeycodeToQueue(KC_ENTER);
      sprintf(buffer,  "Serial No:       %s", getMcuUniqueID());
      addStringToQueue(buffer);
      addKeycodeToQueue(KC_ENTER);
      addKeycodeToQueue(KC_ENTER);
    break;

    case PRINT_BLE:
      sprintf(buffer,  "Profile 1: %02X:%02X:%02X:%02X:%02X:%02X - %s", keyboardconfig.BLEProfileAddr[0][5],
                                                                        keyboardconfig.BLEProfileAddr[0][4],
                                                                        keyboardconfig.BLEProfileAddr[0][3],
                                                                        keyboardconfig.BLEProfileAddr[0][2],
                                                                        keyboardconfig.BLEProfileAddr[0][1],
                                                                        keyboardconfig.BLEProfileAddr[0][0],
                                                                        keyboardconfig.BLEProfileName[0]);
      addStringToQueue(buffer);
      if (keyboardconfig.BLEProfile == 0) addStringToQueue(" (active)");
      addKeycodeToQueue(KC_ENTER);
      sprintf(buffer,  "Profile 2: %02X:%02X:%02X:%02X:%02X:%02X - %s", keyboardconfig.BLEProfileAddr[1][5],
                                                                        keyboardconfig.BLEProfileAddr[1][4],
                                                                        keyboardconfig.BLEProfileAddr[1][3],
                                                                        keyboardconfig.BLEProfileAddr[1][2],
                                                                        keyboardconfig.BLEProfileAddr[1][1],
                                                                        keyboardconfig.BLEProfileAddr[1][0],
                                                                        keyboardconfig.BLEProfileName[1]);
      addStringToQueue(buffer);
      if (keyboardconfig.BLEProfile == 1) addStringToQueue(" (active)");
      addKeycodeToQueue(KC_ENTER);
      sprintf(buffer,  "Profile 3: %02X:%02X:%02X:%02X:%02X:%02X - %s", keyboardconfig.BLEProfileAddr[2][5],
                                                                        keyboardconfig.BLEProfileAddr[2][4],
                                                                        keyboardconfig.BLEProfileAddr[2][3],
                                                                        keyboardconfig.BLEProfileAddr[2][2],
                                                                        keyboardconfig.BLEProfileAddr[2][1],
                                                                        keyboardconfig.BLEProfileAddr[2][0],
                                                                        keyboardconfig.BLEProfileName[2]);
      addStringToQueue(buffer);
      if (keyboardconfig.BLEProfile == 2) addStringToQueue(" (active)");
      addKeycodeToQueue(KC_ENTER);
      addKeycodeToQueue(KC_ENTER);
      ble_gap_addr_t gap_addr;
      gap_addr = bt_getMACAddr();
      sprintf(buffer,  "BT MAC Addr: %02X:%02X:%02X:%02X:%02X:%02X", gap_addr.addr[5], gap_addr.addr[4], gap_addr.addr[3], gap_addr.addr[2], gap_addr.addr[1], gap_addr.addr[0]);
      addStringToQueue(buffer);
      addKeycodeToQueue(KC_ENTER);
      addKeycodeToQueue(KC_ENTER);
    break;

    case PRINT_HELP:
      #ifndef IS_SSK
        addStringToQueue("Pause:               Show battery level on lock LEDs"); addKeycodeToQueue(KC_ENTER);
        addKeycodeToQueue(KC_ENTER);
      #endif
      addStringToQueue("Pause + H:           Type this help"); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + V:           Type version information"); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + B:           Type battery information"); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + P:           Type Bluetooth profile information"); addKeycodeToQueue(KC_ENTER);
      addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + 1-3:         Select Bluetooth profile 1-3"); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + U:           Unpair device from Bluetooth profile"); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + S:           Sleep now (when not on USB)"); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + M:           Enable/disable mouse jiggler (disables auto-sleep if BT)"); addKeycodeToQueue(KC_ENTER);
      #ifdef USE_SOLENOID
        addStringToQueue("Pause + C:           Enable/disable MEGA CLACK (USB only)"); addKeycodeToQueue(KC_ENTER);
      #endif
      addStringToQueue("Pause + Esc:         Firmware update (USB only)"); addKeycodeToQueue(KC_ENTER);
      addKeycodeToQueue(KC_ENTER);
      #ifdef IS_SSK
        addStringToQueue("Pause + +/-:         Brightness up/down"); addKeycodeToQueue(KC_ENTER);
      #else
        addStringToQueue("Pause + Num+/Num-:   Brightness up/down"); addKeycodeToQueue(KC_ENTER);
      #endif
      addStringToQueue("Pause + Up/Down:     Volume up/down"); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + Delete:      Mute/unmute"); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + Left/Right:  Prev/next track"); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + Insert/PgUp: Prev/next track"); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + Home:        Play/pause"); addKeycodeToQueue(KC_ENTER);
      addStringToQueue("Pause + End:         Stop"); addKeycodeToQueue(KC_ENTER);
      addKeycodeToQueue(KC_ENTER);
      #ifndef MAC_KEYMAP
        addStringToQueue("Pause + D:           Type degree symbol"); addKeycodeToQueue(KC_ENTER);
        addKeycodeToQueue(KC_ENTER);
      #endif
    break;

    case SLEEP_NOW:
      if (connectionState != CONNECTION_USB) sleepNow();
    break;

    case MOUSE_JIGGLE:
      if (connectionState != CONNECTION_NONE)
      {
        mouseJiggle = !mouseJiggle;
        switch (connectionState)
        {
          case CONNECTION_USB: usb_sendMouseMove((mouseJiggle ? KC_MS_UP : KC_MS_DOWN), 20); break;
          case CONNECTION_BT: bt_sendMouseMove((mouseJiggle ? KC_MS_UP : KC_MS_DOWN), 20); break;
        }
        #ifdef USE_SPEAKER
          playToneNow((mouseJiggle ? TONE_LOCK_HIGH : TONE_LOCK_LOW), 100);
        #endif
      }
    break;

    case MEGA_CLACK:
      #ifdef USE_SOLENOID
        if (connectionState == CONNECTION_USB) 
        {
          solenoid_toggleMode();
          solenoid_fire();
        }
      #endif
    break;

    case SYM_DEGREE: EXPAND_ALT_CODE(KC_KP_0, KC_KP_1, KC_KP_7, KC_KP_6) break; // Alt 0176 degree symbol

    case BLEPROFILE_1:
      if (connectionState != CONNECTION_USB && keyboardconfig.BLEProfile != 0)
      {
        keyboardconfig.BLEProfile = 0;
        keyboardstate.save2flash = true;
        needReset = true;
      }
    break;

    case BLEPROFILE_2:
      if (connectionState != CONNECTION_USB && keyboardconfig.BLEProfile != 1)
      {
        keyboardconfig.BLEProfile = 1;
        keyboardstate.save2flash = true;
        needReset = true;
      }
    break;

    case BLEPROFILE_3:
      if (connectionState != CONNECTION_USB && keyboardconfig.BLEProfile != 2)
      {
        keyboardconfig.BLEProfile = 2;
        keyboardstate.save2flash = true;
        needReset = true;
      }
    break;
  }
}

/**************************************************************************************************************************/
/**************************************************************************************************************************/
void process_user_special_keys()
{

}

/**************************************************************************************************************************/
// Communication with computer and other boards
/**************************************************************************************************************************/
void sendKeyPresses()
{
  KeyScanner::getReport();                                            // get state data - Data is in KeyScanner::currentReport

  if (KeyScanner::special_key > 0)
  {
    process_user_special_keys();
    KeyScanner::special_key = 0;
  }

  if (KeyScanner::macro > 0)
  {
    process_user_macros(KeyScanner::macro);
    KeyScanner::macro = 0;
  }

  if (!stringbuffer.empty()) // if the macro buffer isn't empty, send the first character of the buffer... which is located at the back of the queue
  {
    uint8_t report[8] = {0, 0, 0 ,0, 0, 0, 0, 0};
    uint16_t keyreport = stringbuffer.back();
    stringbuffer.pop_back();

    report[0] = static_cast<uint8_t>((keyreport & 0xFF00) >> 8);// mods
    report[1] = static_cast<uint8_t>(keyreport & 0x00FF);

    switch (connectionState)
    {
      case CONNECTION_USB: usb_sendKeys(report); break;
      case CONNECTION_BT: bt_sendKeys(report); break;
    }

    delay(keyboardstate.timerkeyscaninterval * 2);

    if (stringbuffer.empty()) // make sure to send an empty report when done...
    {
      report[0] = 0;
      report[1] = 0;

      switch (connectionState)
      {
        case CONNECTION_USB: usb_sendKeys(report); break;
        case CONNECTION_BT: bt_sendKeys(report); break;
      }

      delay(keyboardstate.timerkeyscaninterval * 2);
    }
    else
    {
      uint16_t lookahead_keyreport = stringbuffer.back();
      if (lookahead_keyreport == keyreport) // if the next key is the same, make sure to send a key release before sending it again...
      {
        report[0] = static_cast<uint8_t>((keyreport & 0xFF00) >> 8); // mods
        report[1] = 0;

        switch (connectionState)
        {
          case CONNECTION_USB: usb_sendKeys(report); break;
          case CONNECTION_BT: bt_sendKeys(report); break;
        }

        delay(keyboardstate.timerkeyscaninterval * 2);
      }
    }
   // KeyScanner::processingmacros=0;
  }

  else if ((KeyScanner::reportChanged))  //any new key presses anywhere?
  {
    switch (connectionState)
    {
      case CONNECTION_USB: usb_sendKeys(KeyScanner::currentReport); break;
      case CONNECTION_BT: bt_sendKeys(KeyScanner::currentReport); break;
    }
  }

  else if (KeyScanner::specialfunction > 0)
  {
    process_keyboard_function(KeyScanner::specialfunction);
    KeyScanner::specialfunction = 0;
  }

  else if (KeyScanner::consumerChanged)
  {
    switch (connectionState)
    {
      case CONNECTION_USB: usb_sendMediaKey(KeyScanner::consumer); break;
      case CONNECTION_BT: bt_sendMediaKey(KeyScanner::consumer); break;
    }
  }

  else if (KeyScanner::mouse > 0)
  {
    switch (connectionState)
    {
      case CONNECTION_USB: usb_sendMouseKey(KeyScanner::mouse); break;
      case CONNECTION_BT: bt_sendMouseKey(KeyScanner::mouse); break;
    }
    KeyScanner::mouse = 0;
  }

  #if BLE_PERIPHERAL == 1 | BLE_CENTRAL == 1                            /**************************************************/
    if(KeyScanner::layerChanged || (keyboardstate.timestamp-keyboardstate.lastupdatetime > 1000))     //layer comms
    {
      keyboardstate.lastupdatetime = keyboardstate.timestamp;
      sendlayer(KeyScanner::localLayer);
      KeyScanner::layerChanged = false;                                      // mark layer as "not changed" since last update
    }
  #endif                                                                /**************************************************/
}

/**************************************************************************************************************************/
// put your main code here, to run repeatedly:
/**************************************************************************************************************************/
void loop()
{
  NRF_WDT->RR[0] = WDT_RR_RR_Reload;    // pet watchdog

  if (usb_isConnected())
  {
    if (connectionState != CONNECTION_USB)
    {
      if (bt_isConnected()) bt_disconnect();
      bt_stopAdv();
      // usb_sendBlankReport();

      #ifdef USE_LOCK_LEDS
        analogWrite(LED_NUM_LOCK_PIN, (keyboardstate.lock_leds & KEYBOARD_LED_NUMLOCK ? LOCK_LED_BRIGHTNESS : 0));
        analogWrite(LED_CAPS_LOCK_PIN, (keyboardstate.lock_leds & KEYBOARD_LED_CAPSLOCK ? LOCK_LED_BRIGHTNESS : 0));
        analogWrite(LED_SCROLL_LOCK_PIN, (keyboardstate.lock_leds & KEYBOARD_LED_SCROLLLOCK ? LOCK_LED_BRIGHTNESS : 0));
      #endif

      connectionState = CONNECTION_USB;
    }
  }
  else if (bt_isConnected())
  {
    if (connectionState != CONNECTION_BT)
    {
      Battery::first_vbat = true;
      KeyScanner::setLastPressed(millis());
      keyboardstate.timestamp = millis();
      // bt_sendBlankReport();

      #ifdef USE_LOCK_LEDS
        analogWrite(LED_NUM_LOCK_PIN, (keyboardstate.lock_leds & KEYBOARD_LED_NUMLOCK ? LOCK_LED_BRIGHTNESS : 0));
        analogWrite(LED_CAPS_LOCK_PIN, (keyboardstate.lock_leds & KEYBOARD_LED_CAPSLOCK ? LOCK_LED_BRIGHTNESS : 0));
        analogWrite(LED_SCROLL_LOCK_PIN, (keyboardstate.lock_leds & KEYBOARD_LED_SCROLLLOCK ? LOCK_LED_BRIGHTNESS : 0));
      #endif

      connectionState = CONNECTION_BT;
    }
  }
  else
  {
    if (connectionState != CONNECTION_NONE)
    {
      bt_startAdv();
      Battery::first_vbat = true;
      KeyScanner::setLastPressed(millis());
      keyboardstate.timestamp = millis();
      connectionState = CONNECTION_NONE;
    }

    #ifdef USE_LOCK_LEDS
      // if both USB and Bluetooth are disconnected,
      // flash the number of LEDs corresponding to active BLE profile to show we're awake and waiting for some kind of connection
      if (millis() - blink_timer > 250 && !battShownOnLEDs)
      {
        analogWrite(LED_NUM_LOCK_PIN, (blink_state ? LOCK_LED_BRIGHTNESS : 0));
        analogWrite(LED_CAPS_LOCK_PIN, (blink_state ? (keyboardconfig.BLEProfile > 0 ? LOCK_LED_BRIGHTNESS : 0) : 0));
        analogWrite(LED_SCROLL_LOCK_PIN, (blink_state ? (keyboardconfig.BLEProfile > 1 ? LOCK_LED_BRIGHTNESS : 0) : 0));

        blink_state = !blink_state;
        blink_timer = millis();
      }
    #endif
  }

  #ifdef USE_SPEAKER
    processTones();
  #endif

  if (connectionState != CONNECTION_NONE)
  {
    if (mouseJiggle && (millis() - mouseJiggleTimer) > 30000)
    {
      switch (connectionState)
      {
        case CONNECTION_USB: usb_sendMouseMove((lastMouseJiggleDir ? KC_MS_LEFT : KC_MS_RIGHT), 2); break;
        case CONNECTION_BT: bt_sendMouseMove((lastMouseJiggleDir ? KC_MS_LEFT : KC_MS_RIGHT), 2); break;
      }
      lastMouseJiggleDir = !lastMouseJiggleDir;
      mouseJiggleTimer = millis();
    }
  }

  // none of these things can be done in the timer event
  if (needUnpair)
  {
    char filename[33] = { 0 };
    sprintf(filename, "/adafruit/bond_prph/%02X%02X%02X%02X%02X%02X", keyboardconfig.BLEProfileAddr[keyboardconfig.BLEProfile][0],
                                                                      keyboardconfig.BLEProfileAddr[keyboardconfig.BLEProfile][1],
                                                                      keyboardconfig.BLEProfileAddr[keyboardconfig.BLEProfile][2],
                                                                      keyboardconfig.BLEProfileAddr[keyboardconfig.BLEProfile][3],
                                                                      keyboardconfig.BLEProfileAddr[keyboardconfig.BLEProfile][4],
                                                                      keyboardconfig.BLEProfileAddr[keyboardconfig.BLEProfile][5]);
    InternalFS.remove(filename);

    memset(keyboardconfig.BLEProfileAddr[keyboardconfig.BLEProfile], 0, 6);
    strcpy(keyboardconfig.BLEProfileName[keyboardconfig.BLEProfile], "unpaired");
    keyboardstate.save2flash = true;
    bt_disconnect();
  }
  if (keyboardstate.save2flash)
  {
    saveConfig();
    keyboardstate.save2flash = false;
  }
  if (needFSReset)
  {
    InternalFS.format();
    needReset = true;
  }
  if (needReset) NVIC_SystemReset();

  //delay(100);
};

/**************************************************************************************************************************/
void keyscantimer_callback(TimerHandle_t _handle)
{
  #if MATRIX_SCAN == 1
    scanMatrix();
  #endif

  #if SEND_KEYS == 1
    sendKeyPresses();
  #endif

  #ifdef USE_LOCK_LEDS
    // If layer 1 is active (Pause key held), show battery level on lock LEDs
    if (KeyScanner::localLayer == 2)
    {
      if (!battShownOnLEDs)
      {
        battShownOnLEDs = true;
        showBattOnLEDs();
      }
    }
    else if (battShownOnLEDs)
    {
      // Restore previous LED state
      analogWrite(LED_NUM_LOCK_PIN, (keyboardstate.lock_leds & KEYBOARD_LED_NUMLOCK ? LOCK_LED_BRIGHTNESS : 0));
      analogWrite(LED_CAPS_LOCK_PIN, (keyboardstate.lock_leds & KEYBOARD_LED_CAPSLOCK ? LOCK_LED_BRIGHTNESS : 0));
      analogWrite(LED_SCROLL_LOCK_PIN, (keyboardstate.lock_leds & KEYBOARD_LED_SCROLLLOCK ? LOCK_LED_BRIGHTNESS : 0));
      battShownOnLEDs = false;
    }
  #endif

  #ifdef USE_SPEAKER
    #ifdef IS_SSK
      // for SSK pseudo numpad (layer 2)
      if (KeyScanner::prevLayer == 0 && KeyScanner::localLayer == 4) playToneNow(TONE_LOCK_HIGH, 100);
      else if (KeyScanner::prevLayer == 4 && KeyScanner::localLayer == 0) playToneNow(TONE_LOCK_LOW, 100);
    #endif

    // play high/low tone when lock key state changes
    if (prevLockKeysPressed ^ KeyScanner::lockKeysPressed)
    {
      // we'll use the lock LED state to tell which state we're changing to
      // therefore tone is opposite of current state since it hasn't updated yet
      if (KeyScanner::lockKeysPressed & KEYBOARD_LED_NUMLOCK)    playToneNow((keyboardstate.lock_leds & KEYBOARD_LED_NUMLOCK ? TONE_LOCK_LOW : TONE_LOCK_HIGH), 100);
      if (KeyScanner::lockKeysPressed & KEYBOARD_LED_CAPSLOCK)   playToneNow((keyboardstate.lock_leds & KEYBOARD_LED_CAPSLOCK ? TONE_LOCK_LOW : TONE_LOCK_HIGH), 100);
      if (KeyScanner::lockKeysPressed & KEYBOARD_LED_SCROLLLOCK) playToneNow((keyboardstate.lock_leds & KEYBOARD_LED_SCROLLLOCK ? TONE_LOCK_LOW : TONE_LOCK_HIGH), 100);
    }
    prevLockKeysPressed = KeyScanner::lockKeysPressed;
  #endif

  unsigned long timeSinceLastKeyPress = keyboardstate.timestamp - KeyScanner::getLastPressed();

  #if SLEEP_ACTIVE == 1
    switch (connectionState)
    {
      case CONNECTION_USB:
        // never sleep in this case
      break;

      case CONNECTION_BT:
        if (!mouseJiggle) gotoSleep(timeSinceLastKeyPress, true);
      break;

      case CONNECTION_NONE:
        gotoSleep(timeSinceLastKeyPress, false);
      break;
    }
  #endif

  #if BLE_CENTRAL == 1
    if ((timeSinceLastKeyPress < 10) && (!Bluefruit.Central.connected() && (!Bluefruit.Scanner.isRunning())))
    {
      Bluefruit.Scanner.start(0);                                             // 0 = Don't stop scanning after 0 seconds  ();
    }
  #endif
}

//********************************************************************************************//
//* Battery Monitoring Task - runs infrequently                                              *//
//********************************************************************************************//
void batterytimer_callback(TimerHandle_t _handle)
{
  batterymonitor.updateBattery();
}

//********************************************************************************************//
//* Idle Task - runs when there is nothing to do                                             *//
//* Any impact of placing code here on current consumption?                                  *//
//********************************************************************************************//
// cppcheck-suppress unusedFunction
extern "C" void vApplicationIdleHook(void)
{
  // Don't call any other FreeRTOS blocking API()
  // Perform background task(s) here
  sd_power_mode_set(NRF_POWER_MODE_LOWPWR);
  sd_app_evt_wait();  // puts the nrf52 to sleep when there is nothing to do.  You need this to reduce power consumption. (removing this will increase current to 8mA)
};
