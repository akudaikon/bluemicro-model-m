#ifndef KEYBOARD_CONFIG_H
#define KEYBOARD_CONFIG_H

#include "hardware_variants.h"

#define ARDUINO_NRF52840_FEATHER

#define DEVICE_NAME_M     "IBM Model M SSK"         /**< Name of device. Will be included in the advertising data. */
#define DEVICE_MODEL      "Clacka Clacka McModelM"  /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME "IBM"                     /**< Manufacturer. Will be passed to Device Information Service. */

#define KEYBOARD_SIDE MASTER

/* HARDWARE DEFINITION*/
/* key matrix size */
#define MATRIX_ROWS 8
#define MATRIX_COLS 16

#define MATRIX_HAS_GHOST

#define IS_SSK

#define USE_POWER_LED
  #define LED_POWER_PIN         10

#define USE_SPEAKER
  #define SPEAKER_EN_PIN        12  // PAM8302 uses 4 mA when active!!
  #define SPEAKER_PIN           13

#define USE_SOLENOID
  #define SOLENOID_PIN          9

#define BATTERY_TYPE  BATT_LIPO
#define VBAT_PIN      (A6)

// The keyboard matrix is attached to the following pins:
// row 0: A0, P0.04
// row 1: A1, P0.05
// row 2: A2, P0.30
// row 3: A3, P0.28
// row 4: A4, P0.02
// row 5: A5, P0.03
// row 6: SCK, P0.14
// row 7: MOSI, P0.13
// col 1-8: MCP23107 GPIO B7-0 (reversed!)
// col 9-16: MCP23107 GPIO A0-7
#define MATRIX_COL_PINS { 0 };
#define MATRIX_ROW_PINS { A0, A1, A2, A3, A4, A5, PIN_SPI_SCK, PIN_SPI_MOSI };


#define LAYOUT( \
    K5A,      K5B, K5C, K5D, K5E, K5F, K5G, K5H, K5I, K5J, K5K, K5L, K5M,   K5N, K5O, K5P, \
    \
    K4A, K4B, K4C, K4D, K4E, K4F, K4G, K4H, K4I, K4J, K4K, K4L, K4M, K4N,   K4O, K4P, K4Q, \
    K3A, K3B, K3C, K3D, K3E, K3F, K3G, K3H, K3I, K3J, K3K, K3L, K3M, K3N,   K3O, K3P, K3Q, \
    K2A, K2B, K2C, K2D, K2E, K2F, K2G, K2H, K2I, K2J, K2K, K2L, K2M, K2N,                  \
    K1A, K1B, K1C, K1D, K1E, K1F, K1G, K1H, K1I, K1J, K1K, K1L,      K1M,        K1N,      \
    K0A,      K0B,                K0C,                     K0D,      K0E,   K0F, K0G, K0H  \
) \
{ \
/* 00 */ { KC_NO, KC_NO, K5A,   KC_NO, K5E,   K2F, K5F,   K2G, K5G,   KC_NO, K2L, KC_NO, KC_NO, KC_NO, K1N,   K0B   }, \
/* 01 */ { KC_NO, K1A,   K3A,   K2A,   K5D,   K3F, K4N,   K3G, K3M,   K5H,   K3L, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO }, \
/* 02 */ { K0A,   KC_NO, K4A,   K5B,   K5C,   K4F, K5J,   K4G, K4M,   K5I,   K4L, K3O,   K4O,   K4Q,   K4P,   KC_NO }, \
/* 03 */ { KC_NO, KC_NO, K4B,   K4C,   K4D,   K4E, K5K,   K4H, K4I,   K4J,   K4K, K5L,   K5M,   K3Q,   K3P,   K5N   }, \
/* 04 */ { KC_NO, KC_NO, K3B,   K3C,   K3D,   K3E, KC_NO, K3H, K3I,   K3J,   K3K, KC_NO, KC_NO, KC_NO, KC_NO, K5O   }, \
/* 05 */ { KC_NO, KC_NO, K2B,   K2C,   K2D,   K2E, K3N,   K2H, K2I,   K2J,   K2K, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO }, \
/* 06 */ { K0E,   K1M,   K1C,   K1D,   K1E,   K1F, K2N,   K1I, K1J,   K1K,   K1B, KC_NO, KC_NO, KC_NO, K5P,   KC_NO }, \
/* 07 */ { KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, K1G, K0C,   K1H, KC_NO, KC_NO, K1L, K0G,   K0H,   KC_NO, K0F,   K0D   }, \
}
/*         0      1      2      3      4      5    6      7    8      9      A    B      C      D      E      F       */

// Corresponds to which columns have real keys associated with them (1) vs KC_NO (0)
// Used for anti-ghost algo
#define LAYOUT_REALKEYS \
{ \
/* row 00 */  0b1100010111110100, \
/* row 01 */  0b0000011111111110, \
/* row 02 */  0b0111111111111101, \
/* row 03 */  0b1111111111111100, \
/* row 04 */  0b1000011110111100, \
/* row 05 */  0b0000011111111100, \
/* row 06 */  0b0100011111111111, \
/* row 07 */  0b1101110011100000, \
}
/*          col FEDCBA9876543210 */

#endif /* KEYBOARD_CONFIG_H */
