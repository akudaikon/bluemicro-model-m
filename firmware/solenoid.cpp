#include <bluefruit.h>
#include "keyboard_config.h"
#include "solenoid.h"

#ifdef USE_SOLENOID

static SoftwareTimer solenoid_timer;
static uint8_t solenoid_mode = SOLENOID_MODE_OFF;

void solenoid_init()
{
  pinMode(SOLENOID_PIN, OUTPUT);
  digitalWrite(SOLENOID_PIN, LOW);
}

void solenoid_toggleMode()
{
  solenoid_stop(NULL);
  if (++solenoid_mode >= SOLENOID_MODE_COUNT) solenoid_mode = SOLENOID_MODE_OFF;
}

void solenoid_fire()
{
  if (solenoid_mode == SOLENOID_MODE_OFF) return;

  switch (solenoid_mode)
  {
    default:
    case SOLENOID_MODE_QUIET:
      solenoid_timer.begin(SOLENOID_DWELL_QUIET, solenoid_stop, NULL, false);
    break;

    case SOLENOID_MODE_LOUD:
      solenoid_timer.begin(SOLENOID_DWELL_LOUD, solenoid_stop, NULL, false);
    break;
  }

  digitalWrite(SOLENOID_PIN, HIGH);
  solenoid_timer.start();
}

void solenoid_stop(TimerHandle_t _handle)
{
  solenoid_timer.stop();
  digitalWrite(SOLENOID_PIN, LOW);
}

#endif