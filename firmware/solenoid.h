#ifndef SOLENOID_H
#define SOLENOID_H

#include <bluefruit.h>

#ifdef USE_SOLENOID

  #define SOLENOID_DWELL_QUIET    3
  #define SOLENOID_DWELL_LOUD     15

  enum
  {
    SOLENOID_MODE_OFF,
    SOLENOID_MODE_QUIET,
    SOLENOID_MODE_LOUD,
    SOLENOID_MODE_COUNT
  };

  void solenoid_init();
  void solenoid_toggleMode();
  void solenoid_fire();
  void solenoid_stop(TimerHandle_t _handle);
  void solenoid_task();

#endif

#endif