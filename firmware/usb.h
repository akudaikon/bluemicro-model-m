#ifndef USB_H
#define USB_H

#include <bluefruit.h>

void usb_setup();
bool usb_isConnected();
void usb_wakeup();
void usb_sendBlankReport();
void usb_sendKeys(uint8_t currentReport[8]);
void usb_sendMediaKey(uint16_t keycode);
void usb_sendMouseKey(uint16_t keycode);
void usb_sendMouseMove(uint16_t keycode, uint8_t steps);

void hid_report_callback(uint8_t report_id, hid_report_type_t report_type, uint8_t const* buffer, uint16_t bufsize);

#endif