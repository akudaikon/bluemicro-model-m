#include <queue>
#include <bluefruit.h>
#include "keyboard_config.h"
#include "datastructures.h"
#include "tones.h"

#ifdef USE_SPEAKER

extern PersistentState keyboardconfig;
static std::queue<toneList_t> toneQueue;
static uint32_t toneDelay = 0;

void playTone(toneList_t toneToPlay)
{
  toneQueue.push(toneToPlay);
}

void playToneNow(unsigned int frequency, unsigned long duration)
{
  #ifdef SPEAKER_EN_PIN
    digitalWrite(SPEAKER_EN_PIN, HIGH);
    delay(50);
  #endif

  tone(SPEAKER_PIN, frequency, duration);
  delay(duration);

  #ifdef SPEAKER_EN_PIN
    digitalWrite(SPEAKER_EN_PIN, LOW);
  #endif
}

void playAllQueuedTonesNow()
{
  while (!toneQueue.empty())
  {
    NRF_WDT->RR[0] = WDT_RR_RR_Reload;    // pet watchdog
    processTones();
  }
}

void clearAllQueuedTones()
{
  while (!toneQueue.empty()) toneQueue.pop();
}

void processTones()
{
  if (!toneQueue.empty())
  {
    if (millis() - toneDelay < 750) return;

    toneList_t toneToPlay = toneQueue.front();
    toneQueue.pop();

    #ifdef SPEAKER_EN_PIN
      digitalWrite(SPEAKER_EN_PIN, HIGH);
      delay(50);
    #endif

    switch (toneToPlay)
    {
      case TONE_STARTUP:
        tone(SPEAKER_PIN, NOTE_E4, 50);
        delay(65);
        tone(SPEAKER_PIN, NOTE_A4, 50);
        delay(65);
        tone(SPEAKER_PIN, NOTE_E5, 50);
        delay(65);
      break;

      case TONE_BLE_PROFILE:
        for (uint8_t clicks = 0; clicks < (keyboardconfig.BLEProfile + 1); clicks++)
        {
          tone(SPEAKER_PIN, TONE_A320_CLICK, 15);
          delay(250);
        }
      break;

      case TONE_BLE_CONNECT:
        tone(SPEAKER_PIN, NOTE_B5, 100);
        delay(100);
        tone(SPEAKER_PIN, NOTE_E6, 350);
        delay(350);
      break;

      case TONE_BLE_DISCONNECT:
        tone(SPEAKER_PIN, NOTE_E6, 100);
        delay(100);
        tone(SPEAKER_PIN, NOTE_B5, 350);
        delay(350);
      break;

      case TONE_SLEEP:
        tone(SPEAKER_PIN, NOTE_E5, 50);
        delay(65);
        tone(SPEAKER_PIN, NOTE_A4, 50);
        delay(65);
        tone(SPEAKER_PIN, NOTE_E4, 50);
        delay(65);
      break;
    }

    #ifdef SPEAKER_EN_PIN
      digitalWrite(SPEAKER_EN_PIN, LOW);
    #endif

    toneDelay = millis();
  }
}

#endif
