/*
Copyright 2018-2020 <Pierre Constantineau>

3-Clause BSD License

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "sleep.h"
#include "keymap.h"
#include "nrf52gpio.h"
#include "tones.h"

/**************************************************************************************************************************/
// Prepare sense pins for waking up from complete shutdown
/**************************************************************************************************************************/
void prepareSleep() 
{
  #ifdef USE_POWER_LED
    ledOff(LED_POWER_PIN);
  #endif

  #ifdef USE_LOCK_LEDS
    analogWrite(LED_NUM_LOCK_PIN, 0);
    analogWrite(LED_CAPS_LOCK_PIN, 0);
    analogWrite(LED_SCROLL_LOCK_PIN, 0);
    NRF_PWM2->ENABLE = 0;  // Turn off PWM peripheral
  #endif

  #ifdef USE_SPEAKER
    clearAllQueuedTones();
    playTone(TONE_SLEEP);
    playAllQueuedTonesNow();  // make sure we play everything before going to sleep
  #endif
}

void sleepNow()
{
  prepareSleep();

  delay(3000);    // delay to let any keys be released

  matrixPrepareSleep();
  sd_power_system_off();
}

/**************************************************************************************************************************/
void gotoSleep(unsigned long timesincelastkeypress, bool connected)
{
  // shutdown when unconnected and no keypresses for SLEEPING_DELAY ms
  if ((timesincelastkeypress > SLEEPING_DELAY) && (!connected))
  {
    prepareSleep();
    matrixPrepareSleep();
    sd_power_system_off();
  } 

  // shutdown when unconnected and no keypresses for SLEEPING_DELAY_CONNECTED ms
  if ((timesincelastkeypress > SLEEPING_DELAY_CONNECTED) && (connected))
  {
    prepareSleep();
    matrixPrepareSleep();
    sd_power_system_off();
  } 
}
